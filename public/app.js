(function() {
  'use strict';

  var globals = typeof window === 'undefined' ? global : window;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var aliases = {};
  var has = ({}).hasOwnProperty;

  var endsWith = function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  };

  var _cmp = 'components/';
  var unalias = function(alias, loaderPath) {
    var start = 0;
    if (loaderPath) {
      if (loaderPath.indexOf(_cmp) === 0) {
        start = _cmp.length;
      }
      if (loaderPath.indexOf('/', start) > 0) {
        loaderPath = loaderPath.substring(start, loaderPath.indexOf('/', start));
      }
    }
    var result = aliases[alias + '/index.js'] || aliases[loaderPath + '/deps/' + alias + '/index.js'];
    if (result) {
      return _cmp + result.substring(0, result.length - '.js'.length);
    }
    return alias;
  };

  var _reg = /^\.\.?(\/|$)/;
  var expand = function(root, name) {
    var results = [], part;
    var parts = (_reg.test(name) ? root + '/' + name : name).split('/');
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function expanded(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';
    path = unalias(name, loaderPath);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has.call(cache, dirIndex)) return cache[dirIndex].exports;
    if (has.call(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  require.register = require.define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  require.list = function() {
    var result = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  require.brunch = true;
  require._cache = cache;
  globals.require = require;
})();
/* jshint ignore:start */
(function() {
  var WebSocket = window.WebSocket || window.MozWebSocket;
  var br = window.brunch = (window.brunch || {});
  var ar = br['auto-reload'] = (br['auto-reload'] || {});
  if (!WebSocket || ar.disabled) return;

  var cacheBuster = function(url){
    var date = Math.round(Date.now() / 1000).toString();
    url = url.replace(/(\&|\\?)cacheBuster=\d*/, '');
    return url + (url.indexOf('?') >= 0 ? '&' : '?') +'cacheBuster=' + date;
  };

  var browser = navigator.userAgent.toLowerCase();
  var forceRepaint = ar.forceRepaint || browser.indexOf('chrome') > -1;

  var reloaders = {
    page: function(){
      window.location.reload(true);
    },

    stylesheet: function(){
      [].slice
        .call(document.querySelectorAll('link[rel=stylesheet]'))
        .filter(function(link) {
          var val = link.getAttribute('data-autoreload');
          return link.href && val != 'false';
        })
        .forEach(function(link) {
          link.href = cacheBuster(link.href);
        });

      // Hack to force page repaint after 25ms.
      if (forceRepaint) setTimeout(function() { document.body.offsetHeight; }, 25);
    }
  };
  var port = ar.port || 9485;
  var host = br.server || window.location.hostname || 'localhost';

  var connect = function(){
    var connection = new WebSocket('ws://' + host + ':' + port);
    connection.onmessage = function(event){
      if (ar.disabled) return;
      var message = event.data;
      var reloader = reloaders[message] || reloaders.page;
      reloader();
    };
    connection.onerror = function(){
      if (connection.readyState) connection.close();
    };
    connection.onclose = function(){
      window.setTimeout(connect, 1000);
    };
  };
  connect();
})();
/* jshint ignore:end */
;require.register("js/StoredCheckbox", function(exports, require, module) {

var ls = window.localStorage;

module.exports = function StoredCheckbox(element,options) {

  var savedState;

  this.input = element.getElementsByTagName('input')[0];
  this.label = element.getElementsByTagName('label')[0];
  this.id = this.input.id;
  
  this.unsaved = false;
  savedState  = !!+ls.getItem(this.id);


  this.input.onchange = function (e) {
    this.unsaved = this.input.checked != savedState;
  }.bind(this);


  this.saveState = function () {
    ls.setItem(this.id, savedState = (this.input.checked)?'1':'0');
  }

  this.restore = function () {
    this.input.checked = savedState;
  }

  this.restore();

}
});

;require.register("js/confirm", function(exports, require, module) {
function Confirm(element, options) {

  this.element = document.getElementById(element);
  
  this.init();
}

Confirm.prototype = {
  init: function() {
    this.hide(this.element);
  },

  show: function( ) {
    this.element.style.display = "block";
  },

  hide: function() {
    this.element.style.display = "none";
  },

  confirm: function () {
    var ok = this.element.querySelector('#confirm-ok');
    var cancel = this.element.querySelector('#confirm-cancel');
    ok.className = 'ok';
    cancel.style.display = 'none';

    setTimeout(function() {
      ok.className = ''
      cancel.style.display = 'block';
      this.hide();
    }.bind(this),300)
  },

  ask: function(ok,cancel) {
    var _this = this;
    _this.show();
    
    _this.element.querySelector( "#confirm-ok" ).
    addEventListener( "click", function() {
      _this.confirm();
      ok();
    }, false);
    
    
    _this.element.querySelector( "#confirm-cancel" ).
    addEventListener( "click", function() {
      _this.hide();
      cancel();
    }, false);
  } 
};

module.exports = Confirm;
});

require.register("js/navCollapse", function(exports, require, module) {

function NavCollapse (buttonId) {

  this.button = document.getElementById(buttonId);
  this.nav = document.getElementsByClassName('navigation')[0];

  this.init();

}

NavCollapse.prototype = {

  init:function () {
    this.hide();
    this.button.addEventListener('click',this.toggle.bind(this),false);
  },

  toggle: function() {
    if (this.isExpanded) {
      this.hide();
    } else {
      this.show();
    }
  },

  show: function () {
    var classList = this.nav.className.split(' ');
    if (classList.indexOf('expand') == -1) {
      classList.push('expand');
    }
    this.isExpanded = true;
    this.nav.className = classList.join(' ');
  },

  hide: function () {
    var classList = this.nav.className.split(' ');
    if (classList.indexOf('expand') > 0) {
      classList.splice(classList.indexOf('expand'));
    }
    this.isExpanded = false;
    this.nav.className = classList.join(' ');
  }

}

module.exports = NavCollapse;

});

require.register("main", function(exports, require, module) {


var StoredCheckbox = require('js/StoredCheckbox');
var Confirm = require('js/confirm');
var NavCollapse = require('js/navCollapse');

var confBox = new Confirm('confirmation');
var collapse = new NavCollapse('nav-toggle');

var checkboxes = document.getElementsByClassName('checkbox');
var storedCheckboxes = [];


//checkboxes
for (var i = 0; i < checkboxes.length; i++) {
  var sc = new StoredCheckbox(checkboxes[i]);

  sc.input.addEventListener('change',function (e) {
    var unsaved = storedCheckboxes.some(function (cb) {
      return cb.unsaved;
    });

    if (unsaved) {
      confBox.ask(function() {
        storedCheckboxes.forEach(function (sc) {
          sc.saveState();
        });
      },function() {
        storedCheckboxes.forEach(function (sc) {
          sc.restore();
        });
      });
    } else {
      confBox.hide();
    }

  },false);

  storedCheckboxes.push(sc);
}

//navigation
function locationHashChanged() {  
  var sections = document.getElementsByTagName('section');
  var id = window.location.hash.substr(1) || 'settings';
  var nav = document.getElementsByTagName('nav')[0];
  var buttons = nav.querySelectorAll('li');

  for (var i = 0; i < buttons.length; i++) {
    buttons[i].className = ''
  };
  for (var i = 0; i < sections.length; i++) {
    sections[i].style.display = 'none';
  }

  document.getElementById(id).style.display = 'block';
  window.location.hash = '#'+id;
  nav.querySelector('a[href$='+id+']').parentNode.className = 'active';
  collapse.hide();

}  

locationHashChanged();

window.onhashchange = locationHashChanged;



});


//# sourceMappingURL=app.js.map