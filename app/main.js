

var StoredCheckbox = require('js/StoredCheckbox');
var Confirm = require('js/confirm');
var NavCollapse = require('js/navCollapse');

var confBox = new Confirm('confirmation');
var collapse = new NavCollapse('nav-toggle');

var checkboxes = document.getElementsByClassName('checkbox');
var storedCheckboxes = [];


//checkboxes
for (var i = 0; i < checkboxes.length; i++) {
  var sc = new StoredCheckbox(checkboxes[i]);

  sc.input.addEventListener('change',function (e) {
    var unsaved = storedCheckboxes.some(function (cb) {
      return cb.unsaved;
    });

    if (unsaved) {
      confBox.ask(function() {
        storedCheckboxes.forEach(function (sc) {
          sc.saveState();
        });
      },function() {
        storedCheckboxes.forEach(function (sc) {
          sc.restore();
        });
      });
    } else {
      confBox.hide();
    }

  },false);

  storedCheckboxes.push(sc);
}

//navigation
function locationHashChanged() {  
  var sections = document.getElementsByTagName('section');
  var id = window.location.hash.substr(1) || 'settings';
  var nav = document.getElementsByTagName('nav')[0];
  var buttons = nav.querySelectorAll('li');

  for (var i = 0; i < buttons.length; i++) {
    buttons[i].className = ''
  };
  for (var i = 0; i < sections.length; i++) {
    sections[i].style.display = 'none';
  }

  document.getElementById(id).style.display = 'block';
  window.location.hash = '#'+id;
  nav.querySelector('a[href$='+id+']').parentNode.className = 'active';
  collapse.hide();

}  

locationHashChanged();

window.onhashchange = locationHashChanged;


