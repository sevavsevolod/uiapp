
var ls = window.localStorage;

module.exports = function StoredCheckbox(element,options) {

  var savedState;

  this.input = element.getElementsByTagName('input')[0];
  this.label = element.getElementsByTagName('label')[0];
  this.id = this.input.id;
  
  this.unsaved = false;
  savedState  = !!+ls.getItem(this.id);


  this.input.onchange = function (e) {
    this.unsaved = this.input.checked != savedState;
  }.bind(this);


  this.saveState = function () {
    ls.setItem(this.id, savedState = (this.input.checked)?'1':'0');
  }

  this.restore = function () {
    this.input.checked = savedState;
  }

  this.restore();

}