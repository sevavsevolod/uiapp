
function NavCollapse (buttonId) {

  this.button = document.getElementById(buttonId);
  this.nav = document.getElementsByClassName('navigation')[0];

  this.init();

}

NavCollapse.prototype = {

  init:function () {
    this.hide();
    this.button.addEventListener('click',this.toggle.bind(this),false);
  },

  toggle: function() {
    if (this.isExpanded) {
      this.hide();
    } else {
      this.show();
    }
  },

  show: function () {
    var classList = this.nav.className.split(' ');
    if (classList.indexOf('expand') == -1) {
      classList.push('expand');
    }
    this.isExpanded = true;
    this.nav.className = classList.join(' ');
  },

  hide: function () {
    var classList = this.nav.className.split(' ');
    if (classList.indexOf('expand') > 0) {
      classList.splice(classList.indexOf('expand'));
    }
    this.isExpanded = false;
    this.nav.className = classList.join(' ');
  }

}

module.exports = NavCollapse;
