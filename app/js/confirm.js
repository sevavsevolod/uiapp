function Confirm(element, options) {

  this.element = document.getElementById(element);
  
  this.init();
}

Confirm.prototype = {
  init: function() {
    this.hide(this.element);
  },

  show: function( ) {
    this.element.style.display = "block";
  },

  hide: function() {
    this.element.style.display = "none";
  },

  confirm: function () {
    var ok = this.element.querySelector('#confirm-ok');
    var cancel = this.element.querySelector('#confirm-cancel');
    ok.className = 'ok';
    cancel.style.display = 'none';

    setTimeout(function() {
      ok.className = ''
      cancel.style.display = 'block';
      this.hide();
    }.bind(this),300)
  },

  ask: function(ok,cancel) {
    var _this = this;
    _this.show();
    
    _this.element.querySelector( "#confirm-ok" ).
    addEventListener( "click", function() {
      _this.confirm();
      ok();
    }, false);
    
    
    _this.element.querySelector( "#confirm-cancel" ).
    addEventListener( "click", function() {
      _this.hide();
      cancel();
    }, false);
  } 
};

module.exports = Confirm;